#!/usr/local/bin/python3
import time
import datetime
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import chromedriver_binary

if __name__ == '__main__':
    capabilities = DesiredCapabilities.CHROME.copy()
    # 証明書の警告を無効にする設定
    capabilities['acceptInsecureCerts'] = True

    driver = webdriver.Chrome(desired_capabilities=capabilities)

    # home表示に必要なCookieを設定してリロードする
    driver.get('https://local-www.nt2.puripettapon.net/')
    driver.add_cookie({
        'name': 'AGE_VERIFY', 
        'value': 'ea98ef8f380da351d0a38d28d38af638c759e4da', 
        'domain': '.nt2.puripettapon.net'
    })
    # TODO: popup訴求枠については popupIDが変わったら進行できなくなるので、✕押して消すようなフローを入れたほうがいい
    driver.add_cookie({
        'name': 'cookie_home_popup_promotion_status', 
        'value': '%7B%2220210115_01%22%3A%7B%22access_date%22%3A1611745322%2C%22disabled%22%3A%221%22%7D%7D', 
        'domain': '.nt2.puripettapon.net'
    })
    driver.refresh()
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="header"]/div/div[2]/div[1]/a[1]').click()
    time.sleep(1)

    emailInput = driver.find_element_by_xpath('//*[@id="s-email"]')
    confirmEmailInput = driver.find_element_by_xpath('//*[@id="js-confirm-email"]')
    username = driver.find_element_by_xpath('//*[@id="s-username"]')
    password = driver.find_element_by_xpath('//*[@id="s-password"]')


    now = datetime.datetime.now()
    uniqueText = now.strftime("%Y%m%d-%H%M%S")
    emailText = 'fukusaka-shoki+' + uniqueText + '@kcgrp.jp'
    emailInput.send_keys(emailText)
    confirmEmailInput.send_keys(emailText)
    password.send_keys('dmmPassw0rd')
    username.send_keys(uniqueText)


# classよりもxpathがいいらしい
    driver.find_element_by_xpath('//*[@id="js-input-with-checkbox"]').click()
    time.sleep(5)

    driver.quit()