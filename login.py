#!/usr/local/bin/python3
import time
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import chromedriver_binary

if __name__ == '__main__':
    capabilities = DesiredCapabilities.CHROME.copy()
    capabilities['acceptInsecureCerts'] = True

    driver = webdriver.Chrome(desired_capabilities=capabilities)

    driver.get('https://local-www.nt2.puripettapon.net/')
    driver.add_cookie({
        'name': 'AGE_VERIFY', 
        'value': 'ea98ef8f380da351d0a38d28d38af638c759e4da', 
        'domain': '.nt2.puripettapon.net'
    })
    driver.add_cookie({
        'name': 'cookie_home_popup_promotion_status', 
        'value': '%7B%2220210115_01%22%3A%7B%22access_date%22%3A1611745322%2C%22disabled%22%3A%221%22%7D%7D', 
        'domain': '.nt2.puripettapon.net'
    })
    driver.refresh()
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="header"]/div/div[2]/div[1]/a[2]').click()
    time.sleep(1)

    emailInput = driver.find_element_by_name("mailAddress")
    passwordInput = driver.find_element_by_name("password")

    emailInput.send_keys('fukusaka-shoki@dmm.com')
    passwordInput.send_keys('dmmPassw0rd')

# classよりもxpathがいいらしい
    driver.find_element_by_xpath('//*[@id="login-button"]').click()
    time.sleep(5)

    driver.quit()